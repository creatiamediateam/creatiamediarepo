-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2017 at 03:44 AM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `creatiam_creatiamedia`
--
CREATE DATABASE IF NOT EXISTS `creatiam_creatiamedia` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `creatiam_creatiamedia`;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `productCategoryID` int(11) NOT NULL DEFAULT '0',
  `productName` varchar(50) NOT NULL DEFAULT '0',
  `thumbnailPath` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `productCategoryID`, `productName`, `thumbnailPath`) VALUES
(9, 1, 'bags', 'images/uploaded/20151211205300_0.jpg'),
(11, 1, 'card holder', 'images/uploaded/20151028152033_0.jpg'),
(12, 1, 'pendrive', 'images/uploaded/20151028152050_0.jpg'),
(13, 1, 'coffee mug', 'images/uploaded/20151028152111_0.jpg'),
(14, 1, 'paper weight', 'images/uploaded/20151028152132_0.jpg'),
(15, 1, 'keychain', 'images/uploaded/20151028152147_0.jpg'),
(16, 1, 'wallet', 'images/uploaded/20151028152212_0.jpg'),
(17, 1, 'stationary', 'images/uploaded/20151028152514_0.jpg'),
(18, 1, 'umbrella', 'images/uploaded/20151028152639_0.jpg'),
(20, 1, 'canopy', 'images/uploaded/20151028153426_0.jpg'),
(21, 1, 'promo table', 'images/uploaded/20151028153707_0.jpg'),
(22, 1, 'wall clock', 'images/uploaded/20151028153923_0.jpg'),
(23, 1, 'Table Clock', 'images/uploaded/20151028153934_0.jpg'),
(24, 1, 'pharma gifts', 'images/uploaded/20151028154650_0.jpg'),
(25, 1, 'flask', 'images/uploaded/20151028155348_0.jpg'),
(26, 2, 't-shirt', 'images/uploaded/20151028163117_0.jpg'),
(27, 2, 'cap', 'images/uploaded/20151028163728_0.jpg'),
(28, 2, 'mobile covers', 'images/uploaded/20151028164434_0.jpg'),
(29, 2, 'canvas printing', 'images/uploaded/20151028164959_0.jpg'),
(30, 2, 'canvas drawing', 'images/uploaded/20151028165015_0.jpg'),
(31, 2, 'photo frames', 'images/uploaded/20151028165030_0.jpg'),
(32, 3, 'Business Cards', 'images/uploaded/20151028171506_0.jpg'),
(33, 3, 'table calendar', 'images/uploaded/20151028171526_0.jpg'),
(34, 3, 'roll up display', 'images/uploaded/20151028171543_0.jpg'),
(35, 3, 'pop up display', 'images/uploaded/20151028171558_0.jpg'),
(36, 3, 'plastic and paper bags', 'images/uploaded/20151028171634_0.jpg'),
(37, 4, 'Wedding card', 'images/uploaded/20151124204918_0.jpg'),
(38, 6, 'note pad', 'images/uploaded/20151031153718_0.jpg'),
(40, 1, 'laptop bags', 'images/uploaded/20151031162209_0.jpg'),
(41, 4, 'Invitation Card', 'images/uploaded/20151124204645_0.jpg'),
(42, 4, 'Birthday Cards', 'images/uploaded/20151124204542_0.jpg'),
(43, 4, 'Anniversary  Cards', 'images/uploaded/20151124205136_0.jpg'),
(44, 4, 'Thanks Cards', 'images/uploaded/20151124205646_0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `ID` int(11) NOT NULL,
  `productCategory` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`ID`, `productCategory`) VALUES
(1, 'corporate items'),
(2, 'gifts for you'),
(3, 'business items'),
(4, 'greeting items'),
(5, 'visual media'),
(6, 'office items'),
(7, 'web media');

-- --------------------------------------------------------

--
-- Table structure for table `productmodel`
--

CREATE TABLE `productmodel` (
  `ID` int(11) NOT NULL,
  `productID` int(11) NOT NULL DEFAULT '0',
  `modelName` varchar(50) NOT NULL DEFAULT '0',
  `description` text,
  `modelImagePath` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productmodel`
--

INSERT INTO `productmodel` (`ID`, `productID`, `modelName`, `description`, `modelImagePath`) VALUES
(2, 23, 'Cl01', NULL, 'images/uploaded/20151030200535_0.jpg'),
(3, 23, 'cl02', NULL, 'images/uploaded/20151030152803_0.jpg'),
(4, 23, 'cl03', NULL, 'images/uploaded/20151030152815_0.jpg'),
(5, 23, 'cl04', NULL, 'images/uploaded/20151030152833_0.jpg'),
(9, 9, 'tote01', NULL, 'images/uploaded/20151030195311_0.jpg'),
(10, 9, 'tote02', NULL, 'images/uploaded/20151030195326_0.jpg'),
(11, 9, 'tote03', NULL, 'images/uploaded/20151030195341_0.jpg'),
(12, 9, 'tote04', NULL, 'images/uploaded/20151030195356_0.jpeg'),
(13, 11, 'card01', NULL, 'images/uploaded/20151030200920_0.jpg'),
(14, 11, 'card02', NULL, 'images/uploaded/20151030200933_0.jpg'),
(15, 11, 'card03', NULL, 'images/uploaded/20151030201004_0.jpg'),
(16, 11, 'card04', NULL, 'images/uploaded/20151030201108_0.jpg'),
(17, 12, 'pd01', NULL, 'images/uploaded/20151030202154_0.jpg'),
(18, 12, 'pd02', NULL, 'images/uploaded/20151030202333_0.jpg'),
(19, 12, 'pd03', NULL, 'images/uploaded/20151030202347_0.jpg'),
(20, 12, 'pd04', NULL, 'images/uploaded/20151030202357_0.jpg'),
(25, 40, 'lap01', NULL, 'images/uploaded/20151031162223_0.jpg'),
(26, 40, 'lap02', NULL, 'images/uploaded/20151031162241_0.jpg'),
(27, 40, 'lap03', NULL, 'images/uploaded/20151031162255_0.jpg'),
(28, 40, 'lap04', NULL, 'images/uploaded/20151031162535_0.jpg'),
(29, 22, 'CWL02', NULL, 'images/uploaded/20151116155327_0.jpg'),
(30, 32, 'TEXTURED CARDS', NULL, 'images/uploaded/20151124171753_0.jpg'),
(31, 32, 'SPOT UV CARDS', NULL, 'images/uploaded/20151124172128_0.jpg'),
(32, 26, 'Rubber Print', NULL, 'images/uploaded/20151124173055_0.jpg'),
(33, 26, 'Embroidary', NULL, 'images/uploaded/20151124173410_0.jpg'),
(34, 26, 'Screen Print', NULL, 'images/uploaded/20151124173605_0.jpg'),
(36, 26, 'Digital Image', NULL, 'images/uploaded/20151124173811_0.png'),
(37, 26, 'Photo Printing', NULL, 'images/uploaded/20151124174108_0.jpg'),
(38, 26, 'Jersy', NULL, 'images/uploaded/20151124174226_0.gif'),
(39, 26, 'Uniforms', NULL, 'images/uploaded/20151124174755_0.jpg'),
(40, 31, 'Multi Photo Frame', NULL, 'images/uploaded/20151124194930_0.jpg'),
(41, 31, 'Multi Photo Frame', NULL, 'images/uploaded/20151124195552_0.jpg'),
(42, 31, 'MPF_03', NULL, 'images/uploaded/20151124195746_0.jpg'),
(43, 31, 'love You', NULL, 'images/uploaded/20151124195916_0.jpg'),
(44, 31, 'Family', NULL, 'images/uploaded/20151124200034_0.jpg'),
(45, 0, 'Live Love Laugh', NULL, 'images/uploaded/20151124200224_0.jpg'),
(46, 31, 'Live Love Laugh', NULL, 'images/uploaded/20151124200329_0.jpg'),
(47, 31, 'Friends', NULL, 'images/uploaded/20151124200510_0.jpg'),
(48, 31, 'Kathakali', NULL, 'images/uploaded/20151124200930_0.jpg'),
(49, 31, 'Christ', NULL, 'images/uploaded/20151124201130_0.jpg'),
(50, 31, 'Ganapathy', NULL, 'images/uploaded/20151124201521_0.jpg'),
(51, 24, 'Stethoscope', NULL, 'images/uploaded/20151124202248_0.jpg'),
(52, 0, 'Multi Function String Pen', NULL, 'images/uploaded/20151124202401_0.jpg'),
(53, 24, 'Multi Function Pen', NULL, 'images/uploaded/20151124202457_0.jpg'),
(54, 24, 'Table Pen Stand', NULL, 'images/uploaded/20151124202549_0.jpg'),
(55, 24, 'Blossoming Pen Stand', NULL, 'images/uploaded/20151124202629_0.jpg'),
(56, 24, 'Tourch with Lamp', NULL, 'images/uploaded/20151124202754_0.jpg'),
(57, 24, 'Heart Shape Key Ring', NULL, 'images/uploaded/20151124203020_0.jpg'),
(58, 24, 'Folding Double Mirror', NULL, 'images/uploaded/20151124203114_0.jpg'),
(59, 24, 'Folding Reading Lamp', NULL, 'images/uploaded/20151124203155_0.jpg'),
(60, 13, 'Company Logo', NULL, 'images/uploaded/20151124203434_0.jpg'),
(61, 13, 'Color Cup With Logo', NULL, 'images/uploaded/20151124203618_0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin`
--

CREATE TABLE `tbllogin` (
  `ID` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbllogin`
--

INSERT INTO `tbllogin` (`ID`, `userName`, `password`, `type`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `productmodel`
--
ALTER TABLE `productmodel`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbllogin`
--
ALTER TABLE `tbllogin`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `productmodel`
--
ALTER TABLE `productmodel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `tbllogin`
--
ALTER TABLE `tbllogin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
